package elit.sig.controllers;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {
	@Value("${Arcgis.IpServer}") private static String IP_ARCGIS_SERVER;
	
	
		@RequestMapping("/")
		public String index() {
			
			return "index.html";
		}
		
		
		@RequestMapping("/login")
		public String login() {

			return "login.jsp";
		}
		


}
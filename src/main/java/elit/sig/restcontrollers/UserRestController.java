package elit.sig.restcontrollers;


import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.InetOrgPerson;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import elit.sig.config.UserArcgis;
import elit.sig.config.UserArcgisConfig;



@RestController
public class UserRestController {

	@Value("${Arcgis.IpArcgisServer}") private String IpArcgis; 

	
	@RequestMapping(value="/userSession" , method=RequestMethod.GET)
	public Map<String, String> getInfoUser(HttpServletRequest request){
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	InetOrgPerson person = (InetOrgPerson)(authentication.getPrincipal());
    	
    	String username = person.getUsername();
    	String role= authentication.getAuthorities().toString().substring(6,  authentication.getAuthorities().toString().length()-1);
    	String DD =  person.getDn().split(",ou=")[2].substring(3, person.getDn().split(",ou=")[2].length());
    	String SD =  person.getDn().split(",ou=")[3];
    	String nom = person.getSn();
    	String prenom = person.getGivenName();
    
    	if (UserArcgisConfig.isAutenticate()){

    		

    		String IP_CLIENT = request.getRemoteAddr().toString();
    		if (IP_CLIENT.equals("0:0:0:0:0:0:0:1"))
    		IP_CLIENT = UserArcgis.IP_SERVER;
    		

    		try {
				if (!UserArcgisConfig.searchUser())
				UserArcgisConfig.addUser();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		
    		try {
				UserArcgis.TOKEN_CLIENT = UserArcgisConfig.generateTokenClient(IP_CLIENT);
			} catch (Exception e) {
				e.printStackTrace();
			}

    		}
    	
    	
    	HashMap<String, String> userAttr = new HashMap<>();
    
		    	userAttr.put("nom", nom);
		    	userAttr.put("prenom",prenom);
		    	userAttr.put("DD",DD);
		    	userAttr.put("SD",SD);
		    	userAttr.put("role",role);
		    	userAttr.put("username",username);
		    	userAttr.put("token",UserArcgis.TOKEN_CLIENT);
		    	userAttr.put("ipArcgis",IpArcgis);
		    	
  
    	return userAttr;
	}
	
}

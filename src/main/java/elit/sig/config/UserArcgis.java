package elit.sig.config;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Properties;


public class UserArcgis {
	
	
	public static final String IP_ARCGIS_SERVER =apProperties().get("IpArcgisServer").toString();	
	public static final String USER_ADMIN = apProperties().get("UserAdmin").toString();
	public static final String PASSWORD_ADMIN = apProperties().get("PasswordAdmin").toString();
	public static final String IP_SERVER = apProperties().get("IpServer").toString();
	public static final String PASSWORD_CLIENT = apProperties().get("PasswordClient").toString();
	public static String TOKEN_ADMIN = "";
	public static String TOKEN_CLIENT = "";
	public static final String EXPIRATION = apProperties().get("Expiration").toString();; 
	
	
	
	public static HashMap<String, String> apProperties() {
		
		HashMap<String, String> arcgisProp = new HashMap<>();
	    
				try(FileReader reader = new FileReader("src/main/resources/application.properties")){
				
				Properties properties = new Properties();
				properties.load(reader);
					
		    	arcgisProp.put("UserAdmin", properties.getProperty("Arcgis.UserAdmin"));
		    	arcgisProp.put("PasswordAdmin",properties.getProperty("Arcgis.PasswordAdmin"));
		    	arcgisProp.put("IpServer",properties.getProperty("Arcgis.IpServer"));
		    	arcgisProp.put("IpArcgisServer",properties.getProperty("Arcgis.IpArcgisServer"));
		    	arcgisProp.put("PasswordClient", properties.getProperty("Arcgis.PasswordClient"));
		    	arcgisProp.put("Expiration", properties.getProperty("Arcgis.Expiration"));
				
				} catch (Exception e) {
				e.printStackTrace();
			}	
				
		return arcgisProp;
	}
	
	
	

}


	




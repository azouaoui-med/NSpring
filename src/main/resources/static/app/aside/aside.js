define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/aside/aside.html",
        "config/mapConfig",
        "config/layerConfig",
        "esri/dijit/BasemapGallery",
        "esri/dijit/Legend",
        "esri/dijit/LayerList",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        mapConfig,
        layerConfig,
        BasemapGallery,
        Legend,
        LayerList

    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            startup: function () {

                var basemapContainer = document.createElement("div");
                $(this.domNode).find("#basemapGallery").append(basemapContainer);

                var basemapGallery = new BasemapGallery({
                    showArcGISBasemaps: false,
                    basemaps: mapConfig.basemaps,
                    map: this.map
                }, basemapContainer);
                basemapGallery.startup();
                basemapGallery.on("error", function (msg) {
                    console.log("basemap gallery error:  ", msg);
                });
                basemapGallery.select("algeria");
            
                basemapGallery.on("selection-change",function(evt){
                    console.log("removed from the gallery");
                    this.map.setVisibility(true);
                  });
       
                var legendContainer = document.createElement('div');
                $(this.domNode).find("#legend").append(legendContainer);

                var legendDijit = new Legend({
                    map: this.map
                    //layerInfos: layerConfig.layers
                }, legendContainer);
                legendDijit.startup();

                var layers = document.createElement("div");
                $(this.domNode).find("#layerList").append(layers);
                var layerList = new LayerList({
                    map: this.map,
                    layers: layerConfig.layers
                }, layers);
                layerList.startup();

            }
        });
    });
define({
    root: {
        logoName: "Arcgis",
        mr_ml: "mr",
        help: "help",
        logout: "Logout",
        notification: "Notifications",
        viewAllNotification: "View all notifications",
        language: "عربي",
        datalanguage: 'data-language =ar'
    },
    "ar": true
});
define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/widgets/draw/draw.html",
        "dojo/i18n!app/widgets/draw/nls/local",
        "esri/toolbars/draw",
        "esri/graphic",
        "esri/symbols/SimpleMarkerSymbol",
        "js/events",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        i18n,
        Draw,
        Graphic,
        SimpleMarkerSymbol,
        events

    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            i18n: i18n,

            startup: function () {
                this.inherited(arguments);
                var toolbar = new Draw(this.map, {
                    tooltipOffset: 20,
                    drawTime: 90
                });
                
                toolbar.on("draw-end", this.addToMap);
                events.addEvent("toolbar",toolbar);
                
                $('#btn-draw-point').click($.proxy(function (e) {

                    events.clearAllEvents();
                    toolbar.activate(Draw.POINT);                    

                }, this));

                $('#btn-draw-line').click($.proxy(function (e) {
                    events.clearAllEvents();
                    toolbar.activate(Draw.POLYLINE);
                }, this));
                $('#btn-draw-polygon').click($.proxy(function (e) {
                    events.clearAllEvents();
                    toolbar.activate(Draw.POLYGON);
                }, this));
                $('#btn-draw-reset').click($.proxy(function (e) {
                    this.map.graphics.clear();
                    events.clearAllEvents();
                }, this));
            },
            addToMap: function (evt) {

                console.log("draw end");
                
                var symbol = new SimpleMarkerSymbol({
                    "color": [255, 255, 255, 64],
                    "size": 12,
                    "angle": -30,
                    "xoffset": 0,
                    "yoffset": 0,
                    "type": "esriSMS",
                    "style": "esriSMSCircle",
                    "outline": {
                        "color": [0, 0, 0, 255],
                        "width": 1,
                        "type": "esriSLS",
                        "style": "esriSLSSolid"
                    }
                });

                var graphic = new Graphic(evt.geometry, symbol);
                this.map.graphics.add(graphic);
            }


        });
    });
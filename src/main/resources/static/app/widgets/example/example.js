define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!app/widgets/example/example.html"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        template
    ) {
        return declare([_WidgetBase, _TemplatedMixin], {
            templateString: template,

            startup: function () {
                this.inherited(arguments);
                var dataSet = [{
                        "first_name": "Airi",
                        "last_name": "Satou",
                        "position": "Accountant",
                        "office": "Tokyo",
                        "start_date": "28th Nov 08",
                        "salary": "$162,700"
                    },
                    {
                        "first_name": "Angelica",
                        "last_name": "Ramos",
                        "position": "Chief Executive Officer (CEO)",
                        "office": "London",
                        "start_date": "9th Oct 09",
                        "salary": "$1,200,000"
                    },
                    {
                        "first_name": "Ashton",
                        "last_name": "Cox",
                        "position": "Junior Technical Author",
                        "office": "San Francisco",
                        "start_date": "12th Jan 09",
                        "salary": "$86,000"
                    },
                    {
                        "first_name": "Bradley",
                        "last_name": "Greer",
                        "position": "Software Engineer",
                        "office": "London",
                        "start_date": "13th Oct 12",
                        "salary": "$132,000"
                    }

                ];


                var table = $('#example').DataTable({
                    data: dataSet,
                    responsive: true,
                    columns: [{
                            title: "first name",
                            data: "first_name"
                        },
                        {
                            title: "last name",
                            data: "last_name"
                        },
                        {
                            title: "position",
                            data: "position"
                        },

                        {
                            "render": function (data, type, row, meta) {
                                return `<div class="text-center">
                                        <button class="btn btn-outline-info" data-action="edit"><i class="far fa-edit"></i></button>
                                        <button class="btn btn-outline-danger" data-action="delete"><i class="far fa-trash-alt"></i></button>
                                        </div>`;
                            }
                        }

                    ]
                });

                $('#example').on('click', '[data-action="edit"]', function (e) {
                    console.log(table.row($(this).parents('tr')).data()["salary"]);
                });
                $('#example').on('click', '[data-action="delete"]', function (e) {
                    console.log(table.row($(this).parents('tr')).data()["first_name"]);
                });

                $(this.myButton).on("click",function () {
                    alert("data dojo attach point");
                  })

            }

        });
    });
define([
    "dojo/i18n!config/nls/local",
], function (i18n) {

    return {
        menus: [{
                title: "Example",
                type: 'simple',
                icon: '',
                widget: {
                    title: 'Widget title',
                    icon: '<i class="fa fa-clone" aria-hidden="true"></i>',
                    path: 'app/widgets/example/example'
                }
            },
            {
                title: "Draw",
                type: 'dorpdown',
                icon: '',
                submenus: [{
                        title: "Draw",
                        icon: '',
                        widget: {
                            title: "Draw",
                            icon: '<i class="far fa-folder-open"></i>',
                            path: 'app/widgets/draw/draw'
                        }
                    },
                    {
                        title: "Edit",
                        icon: '',
                        widget: {
                            title: "Edit",
                            icon: '<i class="far fa-folder-open"></i>',
                            path: 'app/widgets/edit/edit'
                        }
                    }
                ]
            }
        ]

    }

});
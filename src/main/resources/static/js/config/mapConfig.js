define([
    "require",
    "esri/map",
    'esri/dijit/Basemap',
    "esri/dijit/BasemapLayer",
    "esri/basemaps",
    "esri/geometry/Extent"
], function (require, Map, Basemap, BasemapLayer, esriBasemaps, Extent) {

    //create a div with id=mapDiv where the map will be displayed
    $('<div/>', {
        id: 'mapDiv',
    }).appendTo('#main');

    // add a custom basemap to the esri basemaps list 
    // this is needed when you want to set a custom basemap (for ex: local basemap) as a default basemap
    esriBasemaps.algeria1 = {
        baseMapLayers: [{
            url: "http://10.5.28.48:6080/arcgis/rest/services/viewer/wilayaTile/MapServer"
        }],
        thumbnailUrl: "http://10.5.28.48:6080/arcgis/rest/services/viewer/wilayaTile/MapServer/tile/4/6/7",
        title: "Algeria tile"
    }
    return {
        //Create a new map 
        map: new Map("mapDiv", {
            basemap: "algeria1",//set default basemap 
            extent: new Extent({
                "xmin": -1386703.1694781692,
                "ymin": 2814897.2825756585,
                "xmax": 2814897.2825756585,
                "ymax": 4610236.542610689,
                spatialReference: {
                    wkid: 102100
                }
            })
        }),
        // a list of custom basemaps that will be used in basemap gallery
        basemaps: [
            new Basemap({
                layers: [new BasemapLayer({
                    url: "http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/FDP_500_1000_WGS/MapServer"
                })],
                id: "algeria",
                title: "Algeria",
                thumbnailUrl: "img/algeria.png"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                })],
                id: "satellite",
                title: "World Imagery",
                thumbnailUrl: "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/4/5/4"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    type: 'WebTiledLayer',
                    url: "https://{subDomain}.tile.openstreetmap.org/{level}/{col}/{row}.png",
                    subDomains: ["a", "b", "c"]
                })],
                id: "osm",
                title: "Open Street Map",
                thumbnailUrl: "https://a.tile.openstreetmap.org/6/31/25.png"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    type: 'WebTiledLayer',
                    url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{level}/{row}/{col}.png"
                })],
                id: "streets",
                title: "World Street Map",
                thumbnailUrl: "https://stamen-tiles.a.ssl.fastly.net/terrain/10/177/409.png"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    type: 'WebTiledLayer',
                    url: "http://{subDomain}.tile.stamen.com/terrain/{level}/{col}/{row}.png",
                    subDomains: ["a", "b", "c", "d"]
                })],
                id: "stamen",
                title: "Stamen",
                thumbnailUrl: "http://a.tile.stamen.com/terrain/6/31/25.png"
            })
        ]

    }
});
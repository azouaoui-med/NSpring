define([
    "require",
    "esri/layers/FeatureLayer",

], function (require, FeatureLayer) {

    return {

        sources: [{
                featureLayer: new FeatureLayer("https://10.5.28.224:6443/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/20"),
                searchFields: ["nom"],
                suggestionTemplate: "${nom}",
                exactMatch: false,
                outFields: ["*"],
                name: "test",
                //labelSymbol: textSymbol,
                placeholder: "test",
                maxResults: 6,
                maxSuggestions: 6,
                enableSuggestions: true,
                minCharacters: 0,
                localSearchOptions: {
                    distance: 5000
                }
            },
            {
                featureLayer: new FeatureLayer("http://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/US_Senators/FeatureServer/0"),
                searchFields: ["Name", "Party"],
                suggestionTemplate: "${Name}, Party: ${Party}",
                exactMatch: false,
                outFields: ["*"],
                name: "Senators2",
                //labelSymbol: textSymbol,
                placeholder: "Senator name2",
                maxResults: 6,
                maxSuggestions: 6,
                enableSuggestions: true,
                minCharacters: 0,
                localSearchOptions: {
                    distance: 5000
                }
            }
        ]

    }
});
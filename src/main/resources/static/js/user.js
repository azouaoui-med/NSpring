define(["dojo/domReady!"], function () {

    return {
    	nom:null,
    	prenom:null,
    	username:null,
    	role:null,
    	DD:null,
    	SD:null,
    	token:null,
    	ipArcgis:null, 
    	initUser: function (f){
    		
                 	$.ajax({ type: "GET",   
                 	         url: "/userSession",   
                 	         async: false,
                 	         success : $.proxy(function(response)
                 	         {
                 	        	this.DD=response.DD;
                 	        	this.SD=response.SD;
                 	        	this.nom=response.nom;
                 	        	this.username=response.username;
                 	        	this.role=response.role;
                 	        	this.prenom=response.prenom;
                 	        	this.token=response.token;
                 	        	this.ipArcgis=response.ipArcgis;
                 	        	f();
                 	        	
                 	         },this)
                 	});
    	}

                 	
    }
});